urllib3
Django
djangorestframework
Pillow

# for mac address field
netaddr

# db client for Postgres
psycopg2-binary

easysnmp
pid
django-guardian

requests
webdavclient
transliterate

# django-xmlview for pay system allpay
-e git://github.com/nerosketch/django-xmlview.git#egg=django-xmlview

# django-bitfield
-e git://github.com/disqus/django-bitfield.git#egg=django-bitfield

# django_cleanup for clean unused media
-e git://github.com/un1t/django-cleanup.git#egg=django_cleanup

# viberbot
-e git://github.com/Viber/viber-bot-python.git#egg=viberbot

celery
redis

django-cors-headers
django-encrypted-model-fields
django-netfields

# pexpect
-e git://github.com/pexpect/pexpect.git#egg=pexpect
django-filter
